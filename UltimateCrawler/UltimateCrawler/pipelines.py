# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import sqlite3
import logging

class UltimatecrawlerPipeline(object):

    def __init__(self):
        self.create_connection()
        self.create_table()

    def create_connection(self):
        self.conn = sqlite3.connect("webpages.db")
        self.curr = self.conn.cursor()

    def create_table(self):
        self.curr.execute("""DROP TABLE IF EXISTS urls""")
        self.curr.execute("""create table urls(
                            url text,
                            input_fields text,
                            select_fields text,
                            httponly integer
				        )""")

    def process_item(self, item, spider):
        self.store_db(item)
        return item

    def store_db(self,item):
        self.curr.execute("""insert into urls values (?,?,?,?)""", (
            item['url'],
            item['input_fields'],
            item['select_fields'],
            item['httponly']
        ))
        self.conn.commit()
