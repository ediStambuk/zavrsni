import sqlite3

conn = sqlite3.connect('webpages.db')  # You can create a new database by changing the name within the quotes
c = conn.cursor() # The database will be saved in the location where your 'py' file is saved

# Create table - CLIENTS
# c.execute("""DROP TABLE IF EXISTS urls""")
# c.execute("""create table urls(
#                             url text,
#                             input_fields text,
#                             select_fields text,
#                             https integer
# 				        )""")
# c.execute('''insert into urls values('https://localhost.com', 'hello', 'bye', 1)''')
c.execute('''SELECT * FROM urls''')
print(c.fetchall())

conn.commit()