# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class UltimatecrawlerItem(scrapy.Item):
    url = scrapy.Field()
    input_fields = scrapy.Field()
    select_fields = scrapy.Field()
    httponly = scrapy.Field()
    pass
