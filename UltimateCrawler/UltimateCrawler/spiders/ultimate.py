# -*- coding: utf-8 -*-
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


from ..items import UltimatecrawlerItem
import urllib2


class UltimateSpider(CrawlSpider):
    name = 'ultimate'
    register = ['signup', 'registracija', 'register', 'login', 'prijava']
    start_urls = ['https://www.hr/katalog']
    rules = (
        Rule(LinkExtractor(allow = ('wwwhr', )),),
        Rule(LinkExtractor(allow = ('wwwhr', )),),
        Rule(LinkExtractor(allow = ('go',), deny=('wwwhr', )), callback='pageParse'),
    )
    #start_urls = ['https://www.amazon.com']
    #start_urls = ['https://www.njuskalo.hr']


    #0 da se dovede multiagent X
    #1.parsat forme i izvadit nazive za više starnica da funkcionira X
    #2.da crawla do sign up-a ili registrationa 2h X
    #3. da se dovede baza 30min X
    #4. da se requesta header 30min X
    #5. da se izvedu zanimljive informacije iz toga i spreme se u bazu 1h X
    #7. pronaci slicne projekte na internetu X wget, i ostali za sniffanje bolje proucit prije pisanja zavrsnog
    #8. prouci je li ti crawler dobro radi po tome je li u bazi ima stranica koje ne pripadaju sekciji o hrvatskoj
    #9. naci nacin kako bi samo jedan registar obisao za jednu stranicu
    #10. da se sve potrebne informacije mapiraju na pocetni link


    def pageParse(self, response):

        #yield response.follow(response.request.url.get(), callback = self.parseFields)
        next_pages = response.css('a::attr(href)')
        for page in next_pages:
            if self.url_has_register(page.get()):
                yield response.follow(page.get(), callback = self.pageParse)

        #if UltimateSpider.register[0] in response.request.url or UltimateSpider.register[1] in response.request.url:
        #self.searchRegisterFields(response)

        # osigurat da može nastavit ako trenutni url ima jedan od pojmova iz liste i gore
        #if not self.url_has_register(str(response.request.url)):
        #    return

        items = UltimatecrawlerItem()

        form = response.css('form')

        input_fields = form.css('input::attr(name)').extract()
        select_fields = form.css('select::attr(name)').extract()

        header = str(self.get_header(response.request.url))
        #self.get_header(response.request.url)
        if "httponly" in header:
            items['httponly'] = 1
        else:
            items['httponly'] = 0

        items['url'] = response.request.url
        items['input_fields'] = ':'.join(input_fields)
        items['select_fields'] = ':'.join(select_fields)

        yield items

    def parseFields(self, response):
        next_pages = response.css('a::attr(href)')
        for page in next_pages:
            if self.url_has_register(page.get()):
                yield response.follow(page.get(), callback = self.pageParse)

    def get_header(self, url):
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        res = opener.open(url)
        return res.info()
    # def searchRegisterFields(self, response):
    #
    #     items = UltimatecrawlerItem()
    #
    #     form = response.css('form')
    #
    #     input_fields = form.css('input::attr(name)').extract()
    #     select_fields = form.css('select::attr(name)').extract()
    #
    #     items['input_fields'] = input_fields
    #     items['select_fields'] = select_fields
    #
    #     yield items

    def url_has_register(self, url):
        for word in UltimateSpider.register:
            if word in url:
                return True
        return False